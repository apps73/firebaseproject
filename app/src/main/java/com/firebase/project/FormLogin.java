package com.firebase.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.auth.FirebaseUser;

public class FormLogin extends AppCompatActivity {
    private TextView tvRegister;
    private TextView tvInfoMsg;
    private EditText etEmail;
    private EditText etPassword;
    private Button btnLogin;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_login);
        getSupportActionBar().hide();
        startViewingByID();
        tvRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switchToRegistrationScreen();
            }
        });
        btnLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(etEmail.getText().toString().length() == 0 ){
                    tvInfoMsg.setText("Preencha o campo E-mail");
                }else if(etPassword.getText().toString().length() == 0){
                    tvInfoMsg.setText("Preencha o campo Senha");
                }else{
                    tvInfoMsg.setText("");
                    loginUser();
                }
            }
        });


    }

    private void loginUser(){
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();

        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if (task.isSuccessful()) {
                    Toast.makeText(FormLogin.this, "Login Realizado com sucesso!", Toast.LENGTH_SHORT).show();
                    switchToMainScreen();
                } else {
                    try {
                        throw task.getException();
                    } catch (FirebaseAuthWeakPasswordException e) {
                        tvInfoMsg.setText("Senha Invalida!");
                    } catch (FirebaseAuthInvalidCredentialsException e) {
                        tvInfoMsg.setText("E-mail ou senha estão incorretos!");
                    } catch (FirebaseNetworkException e) {
                        tvInfoMsg.setText("Sem conexão com a internet!");
                    } catch (Exception e) {
                        tvInfoMsg.setText("Erro ao logar usuario: " + e.getMessage());
                        e.printStackTrace();
                    }
                }
            }
        });
    }

    @Override
    protected void onStart(){
        super.onStart();
        FirebaseUser currentUser = mAuth.getCurrentUser();
        if(currentUser != null){
            switchToMainScreen();
        }
    }

    private void switchToMainScreen(){
        Intent intent = new Intent (FormLogin.this, MainScreen.class);
        startActivity(intent);
    }

    private void startViewingByID(){
        tvRegister = findViewById(R.id.tvRegister);
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnLogin = findViewById(R.id.btnLogin);
        tvInfoMsg = findViewById(R.id.tvInfoMsg);

    }

    private void switchToRegistrationScreen(){
        Intent intent = new Intent(FormLogin.this, FormRegister.class);
        startActivity(intent);
    }
}
