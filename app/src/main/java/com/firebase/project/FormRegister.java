package com.firebase.project;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.FirebaseNetworkException;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthException;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;

public class FormRegister extends AppCompatActivity {
    private EditText etEmail;
    private EditText etPassword;
    private Button btnRegister;
    private TextView tvInfoMsg;

    private FirebaseAuth mAuth = FirebaseAuth.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_form_register);
        getSupportActionBar().hide();
        startViewingByID();

        btnRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if( etEmail.getText().toString().length() == 0 ){
                    tvInfoMsg.setText("Preencha o campo E-mail");
                }else if(etPassword.getText().toString().length() == 0){
                    tvInfoMsg.setText("Preencha o campo Senha");
                }else{
                    tvInfoMsg.setText("");
                    createAccount();
                }

            }
        });
    }

    private void createAccount(){
        String email = etEmail.getText().toString();
        String password = etPassword.getText().toString();
        mAuth.createUserWithEmailAndPassword(email, password).addOnCompleteListener(FormRegister.this, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {

                if (task.isSuccessful()) {
                    Toast.makeText(FormRegister.this, "Registro Realizado com sucesso!", Toast.LENGTH_SHORT).show();
                    switchToLoginScreen();
                } else {
                    try{
                        throw task.getException();
                    }catch (FirebaseAuthWeakPasswordException e){
                        tvInfoMsg.setText("Digite uma senha com no minimo 6 caracteres!");
                    }catch (FirebaseAuthInvalidCredentialsException e) {
                        tvInfoMsg.setText("Por favor digite um e-mail valido!");
                    }catch (FirebaseAuthUserCollisionException e){
                        tvInfoMsg.setText("Este e-amil já foi cadastrado!");
                    }catch (FirebaseNetworkException e) {
                        tvInfoMsg.setText("Sem conexão com a internet!");
                    }catch (Exception e){
                        tvInfoMsg.setText("Erro ao cadastrar usuario: "+e.getMessage());
                        e.printStackTrace();
                    }
                    /*
                    tvInfoMsg.setText("err");
                    String errorCode = ((FirebaseAuthException) task.getException()).getErrorCode();
                    switch (errorCode) {
                        case "ERROR_EMAIL_ALREADY_IN_USE":
                            tvInfoMsg.setText("E-mail já cadastrado!");
                            break;
                        case "ERROR_INVALID_EMAIL":
                            tvInfoMsg.setText("E-mail Informado é invalido!");
                            break;
                        default:
                            tvInfoMsg.setText(errorCode);
                    }
                    */
                }
            }
        });
    }

    private void startViewingByID(){
        etEmail = findViewById(R.id.etEmail);
        etPassword = findViewById(R.id.etPassword);
        btnRegister = findViewById(R.id.btnRegister);
        tvInfoMsg = findViewById(R.id.tvInfoMsg);
    }

    private void switchToLoginScreen(){
        Intent intent = new Intent(FormRegister.this, FormLogin.class);
        startActivity(intent);
    }
}
