package com.firebase.project.ui.gallery;

import android.view.View;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class GalleryViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public GalleryViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is gallery fragment");
    }

    public void openVideo1(View view){
        System.out.print("AAAA");

    }

    public LiveData<String> getText() {
        return mText;
    }
}