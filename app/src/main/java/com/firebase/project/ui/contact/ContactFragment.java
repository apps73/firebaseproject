package com.firebase.project.ui.contact;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import android.content.Intent;
import com.firebase.project.R;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

import static android.text.TextUtils.substring;

public class ContactFragment extends Fragment {

    private ContactViewModel contactViewModel;
    private EditText etContactName;
    private EditText etContactMsg;
    private Button btnSendContact;


    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        contactViewModel =
                ViewModelProviders.of(ContactFragment.this).get(ContactViewModel.class);
        View root = inflater.inflate(R.layout.fragment_contact, container, false);
        final TextView textView = root.findViewById(R.id.text_contact);
        contactViewModel.getText().observe(this, new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        //startViewingByID();
        etContactName = root.findViewById(R.id.etContactName);
        etContactMsg = root.findViewById(R.id.etContactMsg);
        btnSendContact = root.findViewById(R.id.btnSendContact);

        String user = FirebaseAuth.getInstance().getCurrentUser().getEmail();
        int position = user.indexOf("@");
        etContactName.setText( user.substring(0, position) );

        btnSendContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sendEmail(etContactName.getText().toString(), etContactMsg.getText().toString());
                etContactName.setText("");
                etContactMsg.setText(" ");
            }
        });
        return root;
    }


    private void startViewingByID(){
        etContactName = getActivity().findViewById(R.id.etContactName);
        etContactMsg = getActivity().findViewById(R.id.etContactMsg);
        btnSendContact = getActivity().findViewById(R.id.btnSendContact);
    }

    public void sendEmail(String name, String msg){
        final String PACKAGEM_GOOGLEMAIL = "com.google.android.gm";
        Intent email = new Intent(Intent.ACTION_SEND);
        email.putExtra(Intent.EXTRA_EMAIL, new String []{"derexscript@gmail.com"} );
        email.putExtra(Intent.EXTRA_SUBJECT, "Contact Via APP By "+name);
        email.putExtra(Intent.EXTRA_TEXT, msg);
        email.setType("message/rfc822");
        email.setPackage(PACKAGEM_GOOGLEMAIL);
        startActivity(Intent.createChooser(email, " "));
    }
}

